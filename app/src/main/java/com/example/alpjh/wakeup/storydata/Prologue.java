package com.example.alpjh.wakeup.storydata;

import com.example.alpjh.wakeup.StoryActivity;

/**
 * Created by alpjh on 2018-03-07.
 */

public class Prologue {

    public void storySet() {

        StoryActivity.story.add("충성!");
        StoryActivity.story.add("신병이니??");
        StoryActivity.story.add("이병! 최영호! 신병입니다!");
        StoryActivity.story.add("그래 군생활 열심히 하고");
        StoryActivity.story.add("예!");
        StoryActivity.story.add("다나까 안쓰냐?");
        StoryActivity.story.add("죄송합니다!");
        StoryActivity.story.add("죄송하면 다냐? 죄송하면 군생활 끝나?");
        StoryActivity.story.add("시정하겠습니다!");
        StoryActivity.story.add("니가 어떻게 시정할건데?");
        StoryActivity.story.add("...");
        StoryActivity.story.add("이병 나부랭이가 어떻게 시정할거냐고");
        StoryActivity.story.add("내가 묻잖아!");
        StoryActivity.story.add("'시발..'");
        StoryActivity.story.add("영두가 짬을 먹게 만들어 영두를 전역시켜라!");
    }

    public void illSet() {
        StoryActivity.ill.add(0);
        StoryActivity.ill.add(1);
        StoryActivity.ill.add(0);
        StoryActivity.ill.add(1);
        StoryActivity.ill.add(0);
        StoryActivity.ill.add(1);
        StoryActivity.ill.add(0);
        StoryActivity.ill.add(1);
        StoryActivity.ill.add(0);
        StoryActivity.ill.add(1);
        StoryActivity.ill.add(0);
        StoryActivity.ill.add(1);
        StoryActivity.ill.add(1);
        StoryActivity.ill.add(0);
        StoryActivity.ill.add(0);


    }

}




/*
    텍스트를 세팅하는 스트링 어레이 하나와
    이미지를 세팅할 시기를 알려주는 인티저 어레이 하나를 만들어서


        */